package pl.team.publicBoard.entities;

import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@NoArgsConstructor
@Document(collection = "message")
public class Message {
    @Id
    private String id;
    @NonNull
    private String message;
}
