package pl.team.publicBoard.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.AccessType;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.team.publicBoard.services.AES;


@Data
@NoArgsConstructor
@Document(collection = "user")
public class User {
    @Id
    private String id;
    @NonNull
    @Indexed(unique = true)
    private String username;
    @NonNull
    @Indexed(unique = true)
    private String publicKey;
    @NonNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @NonNull
    private String encodedPrivateKey;

    @JsonIgnore
    public boolean isFullFiled() {
        return username != null && publicKey!=null && password != null && encodedPrivateKey != null;
    }

    public boolean areCredentialsFilled(){
        return username != null && password != null;
    }

    public void encryptPassword(){
        password = AES.encrypt(password);
    }
}
