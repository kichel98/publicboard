package pl.team.publicBoard.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.team.publicBoard.entities.Message;

@Repository
public interface MessageRepository extends MongoRepository<Message, Long> {

}
