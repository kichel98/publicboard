package pl.team.publicBoard.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.team.publicBoard.entities.User;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, Long> {

    User findByUsername(String username);

    User findByPublicKey(String publicKey);
}
