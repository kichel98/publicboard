package pl.team.publicBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.team.publicBoard.common.CustomErrorResponse;
import pl.team.publicBoard.entities.Message;
import pl.team.publicBoard.repositories.MessageRepository;

import java.util.List;

import static pl.team.publicBoard.common.CustomErrorResponse.UNFILLED_REQUEST_BODY;

@RestController
@CrossOrigin
@RequestMapping()
public class MessageController {

    @Autowired
    private MessageRepository repository;

    @GetMapping("/messages")
    private List<Message> getAll(){
        return repository.findAll();
    }

    @PostMapping("/message")
    private ResponseEntity<?> createMessage(@RequestBody Message msg){
        if(msg != null && msg.getMessage() !=null && !msg.getMessage().isBlank()) {
            return new ResponseEntity<>(repository.save(msg), HttpStatus.OK);
        }
        return new ResponseEntity<>(new CustomErrorResponse(UNFILLED_REQUEST_BODY), HttpStatus.BAD_REQUEST);
    }

}