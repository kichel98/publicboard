package pl.team.publicBoard.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.team.publicBoard.entities.User;
import pl.team.publicBoard.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserRepository repository;

    @GetMapping("/users")
    private List<User> getAll() {
        return repository.findAll().stream().map(this::removePrivateKey).collect(Collectors.toList());
    }

    private User removePrivateKey(User user) {
        user.setEncodedPrivateKey(null);
        return user;
    }

}