package pl.team.publicBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.team.publicBoard.common.CustomErrorResponse;
import pl.team.publicBoard.entities.User;
import pl.team.publicBoard.repositories.UserRepository;

import static pl.team.publicBoard.common.CustomErrorResponse.*;

@CrossOrigin
@RestController
public class MainController {

    @Autowired
    private UserRepository repository;

    @GetMapping("/")
    private String getRootContent() {
        return "TRY ON /message /user /signin /login";
    }

    @PostMapping("/login")
    private ResponseEntity<?> login(@RequestBody User user) {
        if (user != null && user.areCredentialsFilled()) {
            User dbUser = repository.findByUsername(user.getUsername());
            if (dbUser != null) {
                String dbUserPwd = dbUser.getPassword();
                user.encryptPassword();
                if (dbUserPwd.equals(user.getPassword())) {
                    return new ResponseEntity<>(dbUser, HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(new CustomErrorResponse(LOGIN_FAILED),HttpStatus.UNAUTHORIZED);
    }


    @PostMapping("/signin")
    private ResponseEntity<?> signIn(@RequestBody User user) {
        if (user != null && user.isFullFiled()) {
            User sameUsername = repository.findByUsername(user.getUsername());
            User samePubKey = repository.findByPublicKey(user.getPublicKey());
            if (samePubKey != null && sameUsername != null) {
                return new ResponseEntity<>(new CustomErrorResponse(DUPLICATE_KEY_AND_USERNAME_CODE), HttpStatus.CONFLICT);
            }
            if (sameUsername != null) {
                return new ResponseEntity<>(new CustomErrorResponse(DUPLICATE_USERNAME_CODE), HttpStatus.CONFLICT);
            }
            if (samePubKey != null) {
                return new ResponseEntity<>(new CustomErrorResponse(DUPLICATE_PUBLIC_KEY_CODE), HttpStatus.CONFLICT);
            }
            user.encryptPassword();
            return new ResponseEntity<>(repository.save(user), HttpStatus.OK);
        }
        return new ResponseEntity<>(new CustomErrorResponse(UNFILLED_REQUEST_BODY),HttpStatus.BAD_REQUEST);
    }

}
