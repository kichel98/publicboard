package pl.team.publicBoard.common;

import lombok.Data;

import java.util.HashMap;

@Data
public class CustomErrorResponse {
    public static final Long DUPLICATE_KEY_AND_USERNAME_CODE = 1L;
    public static final Long DUPLICATE_PUBLIC_KEY_CODE = 2L;
    public static final Long DUPLICATE_USERNAME_CODE = 3L;
    public static final Long UNFILLED_REQUEST_BODY = 4L;
    public static final Long LOGIN_FAILED = 5L;

    public static final HashMap<Long, String> CustomErrorMap = new HashMap<>() {{
        put(DUPLICATE_KEY_AND_USERNAME_CODE, "Duplicated username and publicKey values.");
        put(DUPLICATE_PUBLIC_KEY_CODE, "Duplicated publicKey value.");
        put(DUPLICATE_USERNAME_CODE, "Duplicated username value.");
        put(UNFILLED_REQUEST_BODY, "Incomplete user data.");
        put(LOGIN_FAILED, "Username or password is incorrect.");
    }};

    private Long code;
    private String description;

    public CustomErrorResponse(Long errCode) {
        code = errCode;
        description = CustomErrorMap.get(errCode);
    }
}

