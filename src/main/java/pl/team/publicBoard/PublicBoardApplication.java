package pl.team.publicBoard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublicBoardApplication {
	public static void main(String[] args) {
		SpringApplication.run(PublicBoardApplication.class, args);
	}
}
