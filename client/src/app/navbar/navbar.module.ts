import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarRoutingModule } from './navbar-routing.module';
import {RouterModule, Routes} from "@angular/router";
import {VoidComponent} from "../void/void.component";
import {HttpClientModule} from '@angular/common/http';

const appRoutes: Routes = [
  { path: 'login', component: VoidComponent },
  { path: 'register', component: VoidComponent }];


@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    CommonModule,
    NavbarRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ]
})
export class NavbarModule { }
