import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {HttpService} from "../http.service";
import {User} from "../register/register.component";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

  user:User;
  constructor(private router : Router,private httpService: HttpService) {
    httpService.currentUser.subscribe(it => this.user = it)
  }

  ngOnInit() {
  }

  logout() {
    this.httpService.logout()
  }
}
