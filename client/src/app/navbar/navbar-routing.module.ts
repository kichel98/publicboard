import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VoidComponent} from "../void/void.component";

const routes: Routes = [
  {path: '/login', component: VoidComponent},
  {path: '/register', component: VoidComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NavbarRoutingModule { }
