import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {Observable, empty} from 'rxjs';
import {map} from 'rxjs/operators';
import { User } from '../register/register.component';

@Component({
  selector: 'app-board-panel',
  templateUrl: './board-panel.component.html',
  styleUrls: ['./board-panel.component.scss']
})

export class BoardPanelComponent implements OnInit {

  user:User;

  messages: Observable<Promise<string>[]>;
  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.currentUser.subscribe(it => {
      this.user =it;
      this.refresh();
    });
    this.httpService.messageSend.subscribe(it => {
      this.refresh();
    });
  }

  refresh(){
    if(this.user) {
      this.messages = this.httpService.getMessages()
        .pipe(map(msgs => {
          return msgs.map(msg => {
            return this.decrypt(this.httpService, msg.message)
          })
        }));
    }else{
      this.messages = empty();
    }
  }

    async decrypt(httpService: HttpService, message: string): Promise<string>{
      let encryptedAesKey = message.substring(0, 512);
      let msgToDecrypt = message.substring(512);

      function fromHexString(buffer: string) {
        return new Uint8Array(buffer.match(/[\da-f]{2}/gi).map(function(h) {
          return parseInt(h, 16)
        }))
      }

      return window.crypto.subtle.importKey(
        "pkcs8",
        fromHexString(this.httpService.currentUserValue.encodedPrivateKey),//binaryDer,
        {
          name: "RSA-OAEP",
          hash: "SHA-256",
        },
        true,
        ["decrypt"]
      ).then(rsaKey => {
        console.log("1");
        return window.crypto.subtle.decrypt(
          {
            name: "RSA-OAEP",
          },
          rsaKey,
          fromHexString(encryptedAesKey)
        )
      })
        .then(decrypted => {
          console.log("2");
          return window.crypto.subtle.importKey(
            "raw",
            decrypted,
            "AES-GCM",
            true,
            ["encrypt", "decrypt"]
          ).then(aesKey => {
            console.log("3");
            return window.crypto.subtle.decrypt(
              {
                name: "AES-GCM",
                iv: new Uint8Array([155, 57, 251, 30, 54, 130, 195, 47, 193, 142, 136, 156]),
                tagLength: 128,
              },
              aesKey,
              fromHexString(msgToDecrypt)
            );
          })
            .then(it => {
              console.log("4");
              return  new TextDecoder("utf-8").decode(it);
            })
        },(error) => {
          console.log(error);
          return undefined
        })
    }
}
export interface UserMessage{
  message: string;
  id: string;
}
