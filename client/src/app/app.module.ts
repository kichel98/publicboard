import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {Router, RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { BoardPanelComponent } from './board-panel/board-panel.component';
import { SendingPanelComponent } from './sending-panel/sending-panel.component';
import { UsersPanelComponent } from './users-panel/users-panel.component';
import { HeaderComponent } from './shared/header/header.component';
import { UsersListComponent } from './users-panel/users-list/users-list.component';
import { UserListElementComponent } from './users-panel/users-list/user-list-element/user-list-element.component';
import {MatDividerModule} from "@angular/material/divider";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from "@angular/material/icon";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { NavbarComponent } from './navbar/navbar.component';
import { VoidComponent } from './void/void.component';

const appRoutes: Routes = [
  { path: 'login', component: VoidComponent },
  { path: 'register', component: VoidComponent }];

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    BoardPanelComponent,
    SendingPanelComponent,
    UsersPanelComponent,
    HeaderComponent,
    UsersListComponent,
    UserListElementComponent,
    NavbarComponent,
    VoidComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    NoopAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private router : Router) {}
}
