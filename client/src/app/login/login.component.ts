import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {Router} from '@angular/router';
import {isNull} from "util";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private httpService: HttpService, private router: Router) {
  }

  errMsg: string;

  ngOnInit() {
  }

  login(login: string, password: string) {
    var encoder = new TextEncoder();

    function toHexString(buffer) {
      return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    }

    window.crypto.subtle.digest({
        name: "SHA-256",
      },
      encoder.encode(password + "" + login + "mhm")
    ).then(
      hash => {
        var user = <UserLogin>{};
        user.username = login;
        user.password = toHexString(hash);
        this.httpService.login(user, password,
          user => {
            this.router.navigateByUrl('/');
            this.errMsg = undefined;
            console.log(this.errMsg)
          },
          (e) => this.errMsg = e.error.description
        )
      }
    )
  }

  isLogged(): boolean {
    return !isNull(this.httpService.currentUserValue);
  }
}


export interface UserLogin {
  username: string;
  password: string;
}

