import {Component} from '@angular/core';
import {HttpService} from './http.service';
import {User} from './register/register.component';
import {Observable} from 'rxjs';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  user: User;

  constructor(private httpService: HttpService, private router: Router) {
    httpService.currentUser.subscribe(it => this.user = it)
  }

  logout() {
    this.httpService.logout();
  }

}
