import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {HttpService} from '../http.service';
import {User} from '../register/register.component';

@Component({
  selector: 'app-sending-panel',
  templateUrl: './sending-panel.component.html',
  styleUrls: ['./sending-panel.component.scss']
})
export class SendingPanelComponent implements OnInit {

  msg: string = "";

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
  }


  sendMessage() {
    var enc = new TextEncoder();
    var message = enc.encode(this.msg);
    var returnString;

    function toHexString(buffer: ArrayBuffer) {
      return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    }

    function fromHexString(buffer: string) {
      return new Uint8Array(buffer.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
      }))
    }

    this.httpService.mySet.forEach(
      selectedUser => {
        window.crypto.subtle.generateKey({
            name: "AES-GCM",
            length: 256,
          },
          true,
          ["encrypt", "decrypt"]
        )
          .then(function (key) {
            return window.crypto.subtle.encrypt({
                name: "AES-GCM",
                iv: new Uint8Array([155, 57, 251, 30, 54, 130, 195, 47, 193, 142, 136, 156]),
                tagLength: 128,
              },
              key,
              message
            )
              .then(function (encrypted) {
                return window.crypto.subtle.exportKey(
                  "raw",
                  key
                )
                  .then(function (keydata) {
                    //console.log(keydata);
                    return window.crypto.subtle.importKey(
                      "spki",
                      fromHexString(window.atob((selectedUser as User).publicKey)),
                      {
                        name: "RSA-OAEP",
                        hash: {
                          name: "SHA-256"
                        },
                      },
                      true,
                      ["encrypt"]
                    )
                      .then(function (publicKey) {
                        return window.crypto.subtle.encrypt({
                            name: "RSA-OAEP",
                          },
                          publicKey,
                          keydata
                        )
                          .then(function (encryptedAes) {
                            returnString = toHexString(encryptedAes) + toHexString(encrypted)
                          });
                      })
                  });
              });

          }).then(() => {
          this.httpService.sendMessage(returnString,
            () => {
              this.msg = "";
            });
          // this.decryptMessage(returnString,ivvi);
        });
      }
    );
    // var user = < UserLogin > {login:"abc", hahs:"abc"};this.httpService.login(user);
  }
}
