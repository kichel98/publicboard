import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendingPanelComponent } from './sending-panel.component';

describe('SendingPanelComponent', () => {
  let component: SendingPanelComponent;
  let fixture: ComponentFixture<SendingPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendingPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendingPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
