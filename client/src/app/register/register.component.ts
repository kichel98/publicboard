import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpService
} from '../http.service';
import {Router} from "@angular/router";
import {isNull} from "util";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private httpService: HttpService, private router: Router) {
  }

  ngOnInit() {
  }

  errMsg: string;

  register(login: string, password: string, passwordS: string) {
    if(password != passwordS){
      this.errMsg = "Passwords are not equal";
      return;
    }

    var user = <User>{};

    var encoder = new TextEncoder();

    function toHexString(buffer) {
      return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    }

    var user: User;

    window.crypto.subtle.generateKey({
        name: "RSA-OAEP",
        modulusLength: 2048,
        publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
        hash: {
          name: "SHA-256"
        },
      },
      true,
      ["encrypt", "decrypt"]
    ).then(
      keyPair => {
        window.crypto.subtle.exportKey(
          "spki",
          keyPair.publicKey
        ).then(it => user.publicKey = window.btoa(toHexString(it)));
        return window.crypto.subtle.exportKey(
          "pkcs8",
          keyPair.privateKey
        )
      }
    ).then(keydata => {

      var saltBuffer = encoder.encode("1e52d1d3e06489b2");
      var passphraseKey = encoder.encode(password);

      window.crypto.subtle.importKey(
        'raw',
        passphraseKey, {
          generator: undefined,
          hash: undefined,
          length: 0,
          namedCurve: undefined,
          prime: undefined,
          name: 'PBKDF2'
        },
        false,
        ['deriveBits', 'deriveKey']
      ).then(function (key) {
        return window.crypto.subtle.deriveKey({
            "name": 'PBKDF2',
            "salt": saltBuffer,
            "iterations": 1000,
            "hash": 'SHA-256'
          },
          key,

          {
            "name": 'AES-GCM',
            "length": 256
          },
          true,
          ["encrypt", "decrypt"]
        )
      }).then(
        aes => {
          crypto.subtle.encrypt({
            name: "aes-gcm",
            iv: new Uint8Array([155, 57, 251, 30, 54, 130, 195, 47, 193, 142, 136, 156]),
            tagLength: 128
          }, aes, keydata).then(function (cipherText) {
            user.encodedPrivateKey = window.btoa(toHexString(cipherText));
          });
        }
      ).then(
        () => {
          window.crypto.subtle.digest({
              name: "SHA-256",
            },
            encoder.encode(password + "" + login + "mhm")
          )
            .then(function (hash) {
              user.username = login;
              user.password = toHexString(hash);
            }).then(() => {
            this.httpService.addUser(user,
              () => {
                this.router.navigateByUrl('/')
                this.errMsg = null;
              },
              (e) => this.errMsg = e.error.description
            )
          });
        });
    })
  }

  isLogged(): boolean {
    return !isNull(this.httpService.currentUserValue);
  }

}


export interface User {
  username: string;
  publicKey: string;
  encodedPrivateKey: string;
  password: string;
}
