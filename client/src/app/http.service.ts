import {Injectable} from '@angular/core';
import {User} from './register/register.component';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {UserLogin} from './login/login.component';
import {map} from 'rxjs/operators';
import {UserMessage} from './board-panel/board-panel.component';
import {falseIfMissing} from "protractor/built/util";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private messageSendSubject: BehaviorSubject<Object>;
  public messageSend: Observable<Object>;
  public mySet = new Set();

  private readonly SERVER_PATH = "http://localhost:8080";
  private readonly USERS = '/users';

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.messageSendSubject = new BehaviorSubject<Object>({});
    this.messageSend = this.messageSendSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  addUser(user: User, successCallback?, failureCallback?) {
    return this.http.post<User>(this.SERVER_PATH + "/signin", user)
      .subscribe(it => console.log(it), failureCallback, successCallback);
  }

  sendMessage(message: string, successCallback?, failureCallback?) {
    return this.http.post<any>(this.SERVER_PATH + "/message", {message: message}).subscribe(it => {
      console.log(it);
      if (successCallback && it.id) {
        this.messageSendSubject.next({})
        successCallback(it);
      } else if (failureCallback) {
        failureCallback();
      }
    });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  login(user: UserLogin, pass: string, successCallback, failureCallback) {

    var encoder = new TextEncoder();

    function fromHexString(buffer: string) {
      return new Uint8Array(buffer.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
      }))
    }

    function toHexString(buffer) {
      return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    }

    return this.http.post<User>(this.SERVER_PATH + "/login", user)
      .subscribe(user => {
        var saltBuffer = encoder.encode("1e52d1d3e06489b2");
        var passphraseKey = encoder.encode(pass);
        console.log(user);

        window.crypto.subtle.importKey(
          'raw',
          passphraseKey, {
            generator: undefined,
            hash: undefined,
            length: 0,
            namedCurve: undefined,
            prime: undefined,
            name: 'PBKDF2'
          },
          false,
          ['deriveBits', 'deriveKey']
        ).then(key => {
          return window.crypto.subtle.deriveKey({
              "name": 'PBKDF2',
              "salt": saltBuffer,
              "iterations": 1000,
              "hash": 'SHA-256'
            },
            key,
            {
              "name": 'AES-GCM',
              "length": 256
            },
            true,
            ["encrypt", "decrypt"]
          )
        }).then(aes => {
          return window.crypto.subtle.decrypt(
            {
              name: "AES-GCM",
              iv: new Uint8Array([155, 57, 251, 30, 54, 130, 195, 47, 193, 142, 136, 156]),
              tagLength: 128,
            },
            aes,
            fromHexString(window.atob(user.encodedPrivateKey))
          )
        }).then(it => {
          user.encodedPrivateKey = toHexString(it);
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          this.currentUser = this.currentUserSubject.asObservable();
          return user;
        });
      }, failureCallback, successCallback);
  }

  getUsers(): Observable<User[]> {
    return this.http.get(this.SERVER_PATH + this.USERS) as Observable<User[]>;
  }

  getMessages(): Observable<UserMessage[]> {
    return this.http.get(this.SERVER_PATH + "/messages") as Observable<UserMessage[]>
  }
}
