import { Component, OnInit } from '@angular/core';
import {HttpService} from "../http.service";
import {User} from "../register/register.component";

@Component({
  selector: 'app-users-panel',
  templateUrl: './users-panel.component.html',
  styleUrls: ['./users-panel.component.scss']
})
export class UsersPanelComponent implements OnInit {

  users: User[];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getUsers().subscribe((users: User[]) => {
      this.users = users;
    })
  }

}
