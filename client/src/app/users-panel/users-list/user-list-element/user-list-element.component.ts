import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../register/register.component";
import {MatCheckboxChange} from '@angular/material';
import {HttpService} from '../../../http.service';

@Component({
  selector: 'app-user-list-element',
  templateUrl: './user-list-element.component.html',
  styleUrls: ['./user-list-element.component.scss']
})
export class UserListElementComponent implements OnInit {

  @Input()
  user: User;

  constructor(private httpServie: HttpService) { }

  ngOnInit() {
  }


  selectUser($event: MatCheckboxChange) {
    if($event.checked){
      this.httpServie.mySet.add(this.user);
    } else {
      this.httpServie.mySet.delete(this.user);
    }

    //console.log(this.httpServie.mySet)
  }
}
