import {Component, Input, OnInit, Output} from '@angular/core';
import {User} from "../../register/register.component";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  readonly MAX_USERS = 10;
  @Input()
  userList: User[];

  userListFiltered: User[];

  constructor() {
  }

  ngOnInit() {
    var maxUsers = Math.min(this.userList.length,this.MAX_USERS);
    this.userListFiltered = this.userList.slice(0,maxUsers);
  }

  updateFilter(event: any) {
    const filter = event.target.value;
    var temp= this.userList.filter((user => user.username.toLowerCase().includes(filter.toLowerCase())));
    var maxUsers = Math.min(temp.length,this.MAX_USERS);
    this.userListFiltered = temp.slice(0,maxUsers);
  }

}
